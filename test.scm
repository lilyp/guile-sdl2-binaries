(use-modules (sdl2)
             (sdl2 image)
             (sdl2 mixer)
             (sdl2 ttf))

(sdl-init '(timer audio joystick haptic game-controller events))

(image-init)
(image-quit)

(mixer-init '(mp3 ogg flac))
(mixer-quit)

(ttf-init)
(ttf-quit)

(sdl-quit)
